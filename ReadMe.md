# Path Simulation

---
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Path Simulation](#path-simulation)
  - [Layout, controlling the Simulation](#layout-controlling-the-simulation)
    - [Beam](#beam)
    - [Detectors](#detectors)
    - [Phantom](#phantom)
    - [World](#world)
    - [Simulation](#simulation)
  - [Output](#output)

<!-- /code_chunk_output -->
---

This module holds a simple Geant4 simulation that may be used to produce: 

- The parameterisation of the inverse squared kinematic term $`1/(\beta cp)^2`$.
A large cube with a single material is subdivided into many equally thin slices.
At each slice the position, direction and energy of a particle are recorded and
stored in a ROOT file.
- Detector hits with detectors placed upstream and downstream of the phantom. 

![](doc/Layout.png)

## Layout, controlling the Simulation

The default simulation configuration contains no detectors. A valid xml file
must be passed via command line arguments to make use of detectors. Xml files
must provide a root node called `ParameterSet` that may contain any of the
following nodes: 1 x `BeamParameters`, n x `DetectorParameters`, 1 x
`PhantomParameters`, 1 x `WorldParameters`, 1 x `SimulationParameters`.

### Beam

The root node name for beam parameters is `BeamParameters`. A single such node
may be listed. It can contain the following fields:

| Parameter         | Type     | Unit | Defaults to |
|:------------------|:---------|:-----|:------------|
| Energy            | double   | MeV  | 200         |
| FWHM              | double   | mm   | 0           |
| NumberOfPrimaries | unsigned |      | 100         |
| SourceZ           | double   | mm   | 0           |
| ParticleName      | string   |      | proton      |

### Detectors

Many `DetectorParameters` nodes may be added to the xml configuration, each of
which is used to create a detector plane. Detector thickness $`t_z`$ is a
quantity that the simulation calculates based on the material and material
budget: 

```math
t_z = \varepsilon X_0,
```

where $`\varepsilon`$ is the material budget parameter and $`X_0`$ is the
radiation length of the material parameter.

| Parameter      | Type   | Unit | Defaults to |
|:---------------|:-------|:-----|:------------|
| Name           | string |      | Detector    |
| Material       | string |      | G4_Si       |
| MaterialBudget | double |      | 0.005       |
| PlacementZ     | double | mm   | 0           |

### Phantom

One `PhantomParameters` node may be used to characterize the phantom. Due to
convention the phantom always starts at $`z = 0`$. The total length of the
phantom is subdivided into many layers to guarantee a fine path and energy loss
sampling.

| Parameter      | Type     | Unit | Defaults to |
|:---------------|:---------|:-----|:------------|
| Name           | string   |      | Phantom     |
| Material       | string   |      | G4_WATER    |
| Thickness      | double   | mm   | 200         |
| NumberOfLayers | unsigned |      | 100         |

### World

One `WorldParameters` node can be used to modify the world material and the
extent of all volumes in the _x_- and _y_-directions. 

| Parameter | Type   | Unit | Defaults to |
|:----------|:-------|:-----|:------------|
| Name      | string |      | World       |
| Material  | string |      | G4_Galactic |
| WorldSize | double | mm   | 500         |

### Simulation

One `SimulationParameters` node can be used to fine-tune the simulation
environment, such as its name, the location of the output file or the number of
threads.

| Parameter            | Type     | Unit | Defaults to |
|:---------------------|:---------|:-----|:------------|
| Name                 | string   |      | World       |
| OutputDirectory      | string   |      | G4_Galactic |
| OnlyEmPhysics [^1]   | -        |      |             |
| NumberOfThreads [^2] | unsigned |      | 1           |

[^1]: This node does not come with a child value. It is sufficient to put in
`<OnlyEmPhysics />` for a physics list without hadronic interactions.
[^2]: Only relevant if Geant4 was built with MultiThreading.

## Output

![](doc/Output.png)

A root file is produced as the result of the simulation. It contains a single
tree _tracing_ with branches _event_, _x_, _y_, _z_, _energy_ and _isPhantom_. 
As can be seen in the image above, phantom and detector hits are stored on the
same tree. The _isPhantom_ branch is used to distinguish them, it stores a $`1`$
for phantom and a $`0`$ for detector hits. 
Coordinates _x_, _y_ and _z_ are given in millimeters, and energy in mega
electronvolt.