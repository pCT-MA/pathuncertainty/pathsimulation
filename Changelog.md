# Changelog

## v0.2 

### Breaking changes

- Configuration is no longer passed by command line arguments, but by a config
file
- Phantom, World and Detector classes were introduced to more easily instantiate
them
- Output trees are no longer wide, but deep. Previous branches, such as $`x_0`$,
$`x_1`$, … (i.e. columns) are now stored in one branch $`X`$ (as rows) only.
- Tests were introduced to check xml config parsing
- Output is completely controlled by `Path::RunAction`, which has a new method
`void Fill(int event,double x,double y,double z,double e,bool isPhantom) const;`
and no longer exposes column and ntuple ids. 
