#include <G4BaryonConstructor.hh>
#include <G4BosonConstructor.hh>
#include <G4EmStandardPhysics_option3.hh>
#include <G4IonBinaryCascadePhysics.hh>
#include <G4IonConstructor.hh>
#include <G4IonElasticPhysics.hh>
#include <G4LeptonConstructor.hh>
#include <G4MesonConstructor.hh>
#include <G4ShortLivedConstructor.hh>

#include "PathPhysicsList.hpp"

namespace Path
{
    PhysicsList::PhysicsList(int verbosity)
        : G4VModularPhysicsList(),
          mConstructors{new G4EmStandardPhysics_option3(verbosity)/*,
                         new G4IonElasticPhysics(verbosity),
                         new G4IonBinaryCascadePhysics(verbosity)*/}
    {
        SetVerboseLevel(verbosity);
    }

    PhysicsList::~PhysicsList()
    {
        for(auto constructor : mConstructors)
        {
            delete constructor;
        }
    }

    void PhysicsList::ConstructParticle()
    {
        G4BosonConstructor pBosonConstructor;
        pBosonConstructor.ConstructParticle();

        G4LeptonConstructor pLeptonConstructor;
        pLeptonConstructor.ConstructParticle();

        G4MesonConstructor pMesonConstructor;
        pMesonConstructor.ConstructParticle();

        G4BaryonConstructor pBaryonConstructor;
        pBaryonConstructor.ConstructParticle();

        G4IonConstructor pIonConstructor;
        pIonConstructor.ConstructParticle();

        G4ShortLivedConstructor pShortLivedConstructor;
        pShortLivedConstructor.ConstructParticle();

        for(auto constructor : mConstructors)
        {
            constructor->ConstructParticle();
        }
    }

    void PhysicsList::ConstructProcess()
    {
        AddTransportation();
        for(auto constructor : mConstructors)
        {
            constructor->ConstructProcess();
        }
    }

} // namespace Path
