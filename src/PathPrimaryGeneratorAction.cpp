#include <sstream>

#include "G4Box.hh"
#include "G4GeneralParticleSource.hh"
#include "G4LogicalVolume.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh" // MeV
#include "Randomize.hh"       // G4UniformRand

#include "PathDetectorConstruction.hpp"
#include "PathPrimaryGeneratorAction.hpp"
#include "PathRunAction.hpp"

namespace Path
{
    PrimaryGeneratorAction::PrimaryGeneratorAction(RunAction* /**/)
        : G4VUserPrimaryGeneratorAction(),
          fParticleGun(new G4GeneralParticleSource())
    { /**/
    }

    PrimaryGeneratorAction::~PrimaryGeneratorAction() { delete fParticleGun; }

    void PrimaryGeneratorAction::GeneratePrimaries(G4Event* GenerateEvent)
    {
        fParticleGun->GeneratePrimaryVertex(GenerateEvent);
    }
} // namespace Path
