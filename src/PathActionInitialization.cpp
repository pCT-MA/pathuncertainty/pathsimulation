#include <G4UserEventAction.hh>
#include <G4UserSteppingAction.hh>

#include "PathActionInitialization.hpp"
#include "PathPrimaryGeneratorAction.hpp"
#include "PathRunAction.hpp"

namespace Path
{
    ActionInitialization::ActionInitialization() : G4VUserActionInitialization()
    { /**/
    }
    ActionInitialization::~ActionInitialization()
    { /**/
    }

    void ActionInitialization::BuildForMaster() const
    {
        SetUserAction(new RunAction());
    }

    void ActionInitialization::Build() const
    {
        auto RunAction = new Path::RunAction();
        SetUserAction(RunAction);
        SetUserAction(new PrimaryGeneratorAction(RunAction));
        SetUserAction(new G4UserEventAction());
        SetUserAction(new G4UserSteppingAction());
    }
} // namespace Path
