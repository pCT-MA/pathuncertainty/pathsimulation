#include <G4Box.hh>
#include <G4LogicalVolume.hh>
#include <G4NistManager.hh>
#include <G4PVPlacement.hh>
#include <G4PVReplica.hh>
#include <G4RotationMatrix.hh>
#include <G4RunManager.hh>
#include <G4SDManager.hh>
#include <G4SystemOfUnits.hh> // mm
#include <G4VisAttributes.hh>
#include <G4VisManager.hh>

#include "PathDetectorConstruction.hpp"
#include "PathSensitiveDetector.hpp"

namespace Path
{
    World::World(WorldParameters const& parameters, float worldLength)
    {
        auto nistManager = G4NistManager::Instance();
        auto material = nistManager->FindOrBuildMaterial(parameters.mMaterial);

        // World
        auto solid = new G4Box(parameters.mName,
                               0.5 * parameters.mWorldSize + 1 * um,
                               0.5 * parameters.mWorldSize + 1 * um,
                               0.5 * worldLength + 1 * um);
        mLogicalVolume = new G4LogicalVolume(solid, material, parameters.mName);
        mLogicalVolume->SetVisAttributes({{1, 1, 1, 0.1}});
        mPhysicalVolume = new G4PVPlacement(0,
                                            G4ThreeVector(),
                                            mLogicalVolume,
                                            parameters.mName,
                                            0,
                                            false,
                                            0,
                                            true);
    }

    G4LogicalVolume* World::GetLogicalVolume() { return mLogicalVolume; }
    G4VPhysicalVolume* World::GetPhysicalVolume() { return mPhysicalVolume; }

    // ---------------------------------------------------------------------- //

    Phantom::Phantom(G4LogicalVolume* parent,
                     PhantomParameters const& parameters,
                     float worldWidthHeight)
    {
        auto nistManager = G4NistManager::Instance();
        auto material = nistManager->FindOrBuildMaterial(parameters.mMaterial);

        auto bulkSolid = new G4Box(parameters.mName,
                                   0.5 * worldWidthHeight,
                                   0.5 * worldWidthHeight,
                                   0.5 * parameters.mThickness);
        auto bulkLogicalVolume =
                new G4LogicalVolume(bulkSolid, material, parameters.mName);
        bulkLogicalVolume->SetVisAttributes({false});
        new G4PVPlacement(nullptr,
                          {0, 0, 0.5 * parameters.mThickness},
                          bulkLogicalVolume,
                          parameters.mName,
                          parent,
                          false,
                          0,
                          true);

        auto layerSolid = new G4Box(parameters.mName,
                                    0.5 * worldWidthHeight,
                                    0.5 * worldWidthHeight,
                                    0.5 * parameters.LayerThickness());
        mLogicalVolume =
                new G4LogicalVolume(layerSolid, material, parameters.mName);
        mLogicalVolume->SetVisAttributes({{0.4, 0.4, 0.4, 0.5}});

        new G4PVReplica(parameters.mName,
                        mLogicalVolume,
                        bulkLogicalVolume,
                        EAxis::kZAxis,
                        static_cast<G4int>(parameters.mNumberOfLayers),
                        parameters.LayerThickness());
    }

    std::string Phantom::GetName() const { return mLogicalVolume->GetName(); }

    void Phantom::SetSensitiveDetector(SensitiveDetector* sensitiveDetector)
    {
        mLogicalVolume->SetSensitiveDetector(sensitiveDetector);
    }

    // ---------------------------------------------------------------------- //

    Detector::Detector(G4LogicalVolume* parent,
                       DetectorParameters const& parameters,
                       float worldSize)
    {
        auto nistManager = G4NistManager::Instance();
        auto material = nistManager->FindOrBuildMaterial(parameters.mMaterial);
        const auto halfZ = 0.5 * parameters.DetectorThickness();
        auto solid = new G4Box(
                parameters.mName, 0.5 * worldSize, 0.5 * worldSize, halfZ);
        mLogicalVolume = new G4LogicalVolume(solid, material, parameters.mName);
        mLogicalVolume->SetVisAttributes({{0.4, 0.4, 0.4, 0.5}});

        new G4PVPlacement(nullptr,
                          {0, 0, parameters.mPlacementZ * mm},
                          mLogicalVolume,
                          parameters.mName,
                          parent,
                          false,
                          0,
                          true);
    }

    std::string const& Detector::GetName() const
    {
        return mLogicalVolume->GetName();
    }

    void Detector::SetSensitiveDetector(SensitiveDetector* sensitiveDetector)
    {
        mLogicalVolume->SetSensitiveDetector(sensitiveDetector);
    }

    // ---------------------------------------------------------------------- //

    DetectorConstruction::DetectorConstruction(ParameterSet const& parameters)
        : G4VUserDetectorConstruction(), mParameters(parameters)
    { /**/
    }

    DetectorConstruction::~DetectorConstruction()
    { /**/
    }

    float CalculateWorldWidth(ParameterSet const& parameters)
    {
        const auto& minMaxDetectors = std::minmax_element(
                parameters.mDetectorParameters.begin(),
                parameters.mDetectorParameters.end(),
                [](DetectorParameters const& a, DetectorParameters const& b) {
                    return a.mPlacementZ < b.mPlacementZ;
                });

        const auto z0 = std::min(
                {parameters.mBeamParameters.mSourceZ,
                 minMaxDetectors.first->mPlacementZ -
                         0.5f * minMaxDetectors.first->DetectorThickness(),
                 0.f}); // phantom starts at 0

        const auto z1 = std::max(
                {parameters.mBeamParameters.mSourceZ,
                 minMaxDetectors.second->mPlacementZ +
                         0.5f * minMaxDetectors.second->DetectorThickness(),
                 parameters.mPhantomParameters.mThickness});
        return 2 * std::max(z0 < 0 ? -z0 : z0, z1 < 0 ? -z1 : z1);
    }

    G4VPhysicalVolume* DetectorConstruction::Construct()
    {
        const auto& worldSize = mParameters.mWorldParameters.mWorldSize;
        mWorld = new World(mParameters.mWorldParameters,
                           CalculateWorldWidth(mParameters));
        mPhantom = new Phantom(mWorld->GetLogicalVolume(),
                               mParameters.mPhantomParameters,
                               worldSize);
        for(auto detectorParameters : mParameters.mDetectorParameters)
        {
            mDetectors.push_back(new Detector(
                    mWorld->GetLogicalVolume(), detectorParameters, worldSize));
        }
        return mWorld->GetPhysicalVolume();
    }

    // To reduce memory consumption geometry is shared among threads, but
    // sensitive-detectors are not.
    void DetectorConstruction::ConstructSDandField()
    {
        G4SDManager* SDManager = G4SDManager::GetSDMpointer();
        auto phantomSd = new SensitiveDetector(mPhantom->GetName());
        mPhantom->SetSensitiveDetector(phantomSd);
        SDManager->AddNewDetector(phantomSd);

        for(auto detector : mDetectors)
        {
            auto detectorSd = new SensitiveDetector(detector->GetName());
            detector->SetSensitiveDetector(detectorSd);
            SDManager->AddNewDetector(detectorSd);
        }
    }

    ParameterSet const& DetectorConstruction::GetParameters() const
    {
        return mParameters;
    }
} // namespace Path
