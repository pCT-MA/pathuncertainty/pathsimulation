#include <map>

#include <G4Event.hh>
#include <G4RunManager.hh>
#include <G4SDManager.hh>
#include <g4root.hh> // G4AnalysisManager

#include "PathDetectorConstruction.hpp"
#include "PathHit.hpp"
#include "PathRun.hpp"
#include "PathRunAction.hpp"
#include "PathSimulation.hpp"

namespace Path
{
    Run::Run() : eventNr(0)
    {
        G4SDManager* sdManager = G4SDManager::GetSDMpointer();
        const auto detectorConstruction =
                dynamic_cast<const Path::DetectorConstruction*>(
                        G4RunManager::GetRunManager()
                                ->GetUserDetectorConstruction());
        const auto parameters = detectorConstruction->GetParameters();
        mPhantomHitCollectionId =
                sdManager->GetCollectionID(parameters.mPhantomParameters.mName);
        mDetectorHitCollectionIds.resize(parameters.mDetectorParameters.size());
        for(std::size_t i = 0; i < mDetectorHitCollectionIds.size(); i++)
        {
            mDetectorHitCollectionIds[i] = sdManager->GetCollectionID(
                    parameters.mDetectorParameters.at(i).mName);
        }
    }

    Run::~Run()
    { /**/
    }

    void Run::RecordEvent(const G4Event* event)
    {
        G4HCofThisEvent* HitsCollectionOfThisEvent = event->GetHCofThisEvent();
        if(!HitsCollectionOfThisEvent)
        {
            G4Exception("IdealUserEventAction::EndOfEventAction()",
                        "NoHitsCollectionOfThisEventFound",
                        JustWarning,
                        "No hits collection of this event found!");
            return;
        }

        // Get hits of custom hitscollection class
        auto phantomHC = dynamic_cast<HitsCollection_t*>(
                HitsCollectionOfThisEvent->GetHC(mPhantomHitCollectionId));
        auto castFailed = phantomHC == nullptr;
        std::vector<HitsCollection_t*> detectorHCs(
                mDetectorHitCollectionIds.size());
        for(std::size_t i = 0; i < detectorHCs.size(); i++)
        {
            detectorHCs[i] = dynamic_cast<HitsCollection_t*>(
                    HitsCollectionOfThisEvent->GetHC(
                            mDetectorHitCollectionIds.at(i)));
            castFailed |= detectorHCs.at(i) == nullptr;
        }

        if(!castFailed)
        {
            const auto currentRun =
                    G4RunManager::GetRunManager()->GetUserRunAction();
            const auto runAction =
                    dynamic_cast<const Path::RunAction*>(currentRun);

            const std::size_t numberOfPhantomHits = phantomHC->entries();

            // hits in the four detectors
            std::map<std::size_t, Path::Hit*> detectorHits;
            for(std::size_t i = 0; i < detectorHCs.size(); i++)
            {
                const std::size_t numberOfHits = detectorHCs.at(i)->entries();
                for(std::size_t j = 0; j < numberOfHits; j++)
                {
                    const auto hit = dynamic_cast<Path::Hit*>(
                            detectorHCs.at(i)->GetHit(j));
                    if(hit != nullptr)
                    {
                        if(detectorHits.find(i) == detectorHits.end() ||
                           detectorHits.at(i)->GetPosition().z() >
                                   hit->GetPosition().z())
                        {
                            detectorHits[i] = hit;
                        }
                    }
                }
            }
            // if all data available: store output
            if(detectorHits.size() == detectorHCs.size())
            {
                for(std::size_t i = 0; i < numberOfPhantomHits; i++)
                {
                    const auto hit =
                            dynamic_cast<Path::Hit*>(phantomHC->GetHit(i));
                    /* A counter is used even though `event->GetEventID()` is a
                    unique identifier. The event ID series may contain jumps
                    when particles do not reach all detectors. */
                    runAction->Fill(eventNr,
                                    hit->GetPosition().x(),
                                    hit->GetPosition().y(),
                                    hit->GetPosition().z(),
                                    hit->GetEnergy(),
                                    true);
                }

                for(const auto kvp : detectorHits)
                {
                    auto hit = kvp.second;
                    runAction->Fill(eventNr,
                                    hit->GetPosition().x(),
                                    hit->GetPosition().y(),
                                    hit->GetPosition().z(),
                                    hit->GetEnergy(),
                                    false);
                }
                ++eventNr;
            }
        }

        G4Run::RecordEvent(event);
    }
} // namespace Path
