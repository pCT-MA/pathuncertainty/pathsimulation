#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4GeneralParticleSource.hh"
#include "G4RunManager.hh"
#include "G4VisManager.hh"

#include "PathHit.hpp"
#include "PathPrimaryGeneratorAction.hpp"

namespace Path
{
    G4ThreadLocal G4Allocator<Path::Hit>* gHitsAllocator = nullptr;

    Hit::Hit() : G4VHit(), fPosition{}, fEnergy(0), fCopyNumber(0)
    { /**/
    }

    Hit::Hit(const Hit& other)
    {
        fPosition = other.fPosition;
        fEnergy = other.fEnergy;
        fCopyNumber = other.fCopyNumber;
    }

    Hit::~Hit()
    { /**/
    }

    const Hit& Hit::operator=(const Hit& other)
    {
        fPosition = other.fPosition;
        fEnergy = other.fEnergy;
        fCopyNumber = other.fCopyNumber;

        return *this;
    }

    G4int Hit::operator==(const Hit& OtherHit) const
    {
        return (this == &OtherHit) ? 1 : 0;
    }

    void Hit::Draw()
    {
        G4VVisManager* VisManager = G4VVisManager::GetConcreteInstance();
        // Hits are marked as small circles in UI mode
        if(VisManager != nullptr)
        {
            G4Colour colour(1., 0., 0.);

            auto userGenerator =
                    dynamic_cast<const Path::PrimaryGeneratorAction*>(
                            G4RunManager::GetRunManager()
                                    ->GetUserPrimaryGeneratorAction());
            if(userGenerator != nullptr)
            {
                const auto maxEnergy =
                        userGenerator->GetParticleGun()->GetParticleEnergy();
                colour.SetRed(fEnergy / maxEnergy);
            }

            G4Circle Circle(fPosition);
            Circle.SetScreenSize(5.);
            Circle.SetFillStyle(G4Circle::filled);
            G4VisAttributes attribs(colour);
            Circle.SetVisAttributes(attribs);
            VisManager->Draw(Circle);
        }
    }
} // namespace Path
