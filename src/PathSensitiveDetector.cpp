#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"

#include "PathDetectorConstruction.hpp"
#include "PathSensitiveDetector.hpp"
#include "PathSimulation.hpp"

namespace Path
{
    SensitiveDetector::SensitiveDetector(std::string const& detectorName)
        : G4VSensitiveDetector(detectorName), fHitsCollection(nullptr),
          fHitCollectionID(-1)
    {
        collectionName.insert(detectorName);
    }

    SensitiveDetector::~SensitiveDetector()
    { /**/
    }

    void
    SensitiveDetector::Initialize(G4HCofThisEvent* HitCollectionOfThisEvent)
    {
        fHitsCollection =
                new HitsCollection_t(SensitiveDetectorName, collectionName[0]);
        if(fHitCollectionID < 0)
        {
            fHitCollectionID = G4SDManager::GetSDMpointer()->GetCollectionID(
                    fHitsCollection);
        }

        HitCollectionOfThisEvent->AddHitsCollection(fHitCollectionID,
                                                    fHitsCollection);
    }

    G4bool SensitiveDetector::ProcessHits(G4Step* Step,
                                          G4TouchableHistory* /**/)
    {
        // spawn a hit object and transfer the properties
        if(Step->GetTrack()->GetParentID() == 0 && // filter non-primaries
           Step->GetPreStepPoint()->GetTouchable() !=
                   // filter non boundary hits
                   Step->GetPostStepPoint()->GetTouchable())
        {
            auto kineticEnergy = Step->GetPostStepPoint()->GetKineticEnergy();

            auto NewHit = new Path::Hit();
            NewHit->SetPosition(Step->GetPostStepPoint()->GetPosition());
            NewHit->SetEnergy(kineticEnergy);
            NewHit->SetCopyNr(
                    Step->GetPostStepPoint()->GetPhysicalVolume()->GetCopyNo());

            if(fHitsCollection != nullptr)
            {
                fHitsCollection->insert(NewHit);
                return true;
            }
        }
        return false;
    }
} // namespace Path
