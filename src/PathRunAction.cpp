#include <cmath>
#include <ctime>
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4WorkerThread.hh"
#include "g4root.hh" // G4AnalysisManager

#include "PathDetectorConstruction.hpp"
#include "PathRun.hpp"
#include "PathRunAction.hpp"
#include "PathSimulation.hpp"
#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#endif

namespace Path
{
    RunAction::RunAction() : G4UserRunAction()
    {
        G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
        analysisManager->SetVerboseLevel(0);
        analysisManager->SetNtupleMerging(true, 0, false);
    }

    RunAction::~RunAction()
    {
        G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
        analysisManager->Write();
        analysisManager->CloseFile();
        G4cout << "Closed output file: " << analysisManager->GetFileName()
               << G4endl;

        delete G4AnalysisManager::Instance();
    }

    G4Run* RunAction::GenerateRun() { return new Run(); }

    void RunAction::BeginOfRunAction(const G4Run* /**/)
    {
        G4RunManager::GetRunManager()->SetPrintProgress(0);

        auto analysisManager = G4AnalysisManager::Instance();
        if(!analysisManager->IsOpenFile())
        {
            analysisManager->OpenFile();
        }

        mTracingNTupleId = analysisManager->CreateNtuple("tracing", "tracing");
        mEventColumnId =
                analysisManager->CreateNtupleIColumn(mTracingNTupleId, "event");
        mXColumnId =
                analysisManager->CreateNtupleDColumn(mTracingNTupleId, "x");
        mYColumnId =
                analysisManager->CreateNtupleDColumn(mTracingNTupleId, "y");
        mZColumnId =
                analysisManager->CreateNtupleDColumn(mTracingNTupleId, "z");
        mEColumnId = analysisManager->CreateNtupleDColumn(mTracingNTupleId,
                                                          "energy");
        mIsPhantomColumnId = analysisManager->CreateNtupleIColumn(
                mTracingNTupleId, "isPhantom");
        analysisManager->FinishNtuple(mTracingNTupleId);
    }

    void RunAction::Fill(int event,
                         double x,
                         double y,
                         double z,
                         double e,
                         bool isPhantom) const
    {
        auto analysisManager = G4AnalysisManager::Instance();
        analysisManager->FillNtupleIColumn(
                mTracingNTupleId, mEventColumnId, event);
        analysisManager->FillNtupleDColumn(mTracingNTupleId, mXColumnId, x);
        analysisManager->FillNtupleDColumn(mTracingNTupleId, mYColumnId, y);
        analysisManager->FillNtupleDColumn(mTracingNTupleId, mZColumnId, z);
        analysisManager->FillNtupleDColumn(mTracingNTupleId, mEColumnId, e);
        analysisManager->FillNtupleIColumn(
                mTracingNTupleId, mIsPhantomColumnId, isPhantom ? 1 : 0);
        analysisManager->AddNtupleRow(mTracingNTupleId);
    }
} // namespace Path
