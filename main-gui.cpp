#include <G4RunManager.hh>
#include <G4UIExecutive.hh>
#include <G4UImanager.hh>
#include <G4ViewParameters.hh>
#include <G4VisExecutive.hh>
#include <QGSP_BIC.hh>

#include "PathActionInitialization.hpp"
#include "PathDetectorConstruction.hpp"
#include "PathPhysicsList.hpp"
#include "PathSimulation.hpp"

int main(int argc, char** argv)
{
    using namespace Path;
    ParameterSet parameters;
    if(argc > 1)
    {
        parameters = XMLParameters(argv[1]);
    }

    // do we really need to set up the RNG?
    auto rngEngine = new CLHEP::RanecuEngine();
    CLHEP::HepRandom::setTheEngine(rngEngine);
    CLHEP::HepRandom::setTheSeed(time(0));

    auto detectorConstruction = new DetectorConstruction(parameters);
    G4VModularPhysicsList* physicsList =
            parameters.mSimulationParameters.mOnlyEMPhysics
                    ? static_cast<G4VModularPhysicsList*>(new PhysicsList())
                    : static_cast<G4VModularPhysicsList*>(new QGSP_BIC(0));
    auto actionInitialisation = new ActionInitialization();
    auto runManager = new G4RunManager();
    auto uiManager = G4UImanager::GetUIpointer();
    auto visManager = new G4VisExecutive("errors");
    auto userInterface = new G4UIExecutive(argc, argv, "OGL");

    runManager->SetVerboseLevel(0);
    runManager->SetUserInitialization(detectorConstruction);
    runManager->SetUserInitialization(physicsList);
    runManager->SetUserInitialization(actionInitialisation);

    visManager->SetVerboseLevel(0);
    visManager->Initialize();

    uiManager->ApplyCommand("/vis/open OGL");
    runManager->Initialize();
    uiManager->ApplyCommand("/vis/scene/create");
    uiManager->ApplyCommand("/vis/scene/add/volume " +
                            parameters.mWorldParameters.mName);
    uiManager->ApplyCommand("/vis/drawVolume");
    uiManager->ApplyCommand("/vis/viewer/set/defaultColour black");
    uiManager->ApplyCommand("/vis/viewer/set/background white");
    uiManager->ApplyCommand("/vis/viewer/set/style surface");
    uiManager->ApplyCommand("/vis/viewer/set/edge 1");
    uiManager->ApplyCommand("/vis/viewer/set/viewpointVector -1 0 0");
    uiManager->ApplyCommand("/vis/viewer/set/zoom 1.25");
    uiManager->ApplyCommand("/vis/viewer/set/panTo 0 0 mm");
    uiManager->ApplyCommand("/vis/scene/add/axes 0 0 0 10 cm auto 0");
    uiManager->ApplyCommand("/tracking/storeTrajectory 1");
    uiManager->ApplyCommand("/vis/scene/add/trajectories");
    uiManager->ApplyCommand("/vis/scene/add/hits");
    uiManager->ApplyCommand("/vis/scene/endOfEventAction accumulate 1000");

    uiManager->ApplyCommand(
            "/analysis/setFileName " +
            parameters.mSimulationParameters.SimulationFileName());

    uiManager->ApplyCommand(
            "/gps/particle " +
            (parameters.mBeamParameters.ParticleIsIon()
                     ? "ion"
                     : parameters.mBeamParameters.mParticleName));
    if(parameters.mBeamParameters.ParticleIsIon())
    {
        uiManager->ApplyCommand(
                "/gps/ion " +
                std::to_string(parameters.mBeamParameters.IonAtomicNumber()) +
                " " +
                std::to_string(parameters.mBeamParameters.IonMassNumber()) +
                " " + std::to_string(parameters.mBeamParameters.IonCharge()));
    }
    if(parameters.mBeamParameters.mFullWidthHalfMaximumInMM > 0)
    {
        uiManager->ApplyCommand("/gps/pos/type Beam");
        uiManager->ApplyCommand("/gps/pos/shape Circle");
        uiManager->ApplyCommand("/gps/pos/radius 0. mm");
        uiManager->ApplyCommand(
                "/gps/pos/sigma_r " +
                std::to_string(
                        parameters.mBeamParameters.mFullWidthHalfMaximumInMM) +
                " mm");
    }
    else
    {
        uiManager->ApplyCommand("/gps/pos/type Point");
    }
    uiManager->ApplyCommand(
            "/gps/pos/centre 0 0 " +
            std::to_string(parameters.mBeamParameters.mSourceZ) + " mm");
    uiManager->ApplyCommand("/gps/ang/type planar");
    uiManager->ApplyCommand("/gps/direction 0 0 1");
    uiManager->ApplyCommand("/gps/ene/mono " +
                            std::to_string(parameters.mBeamParameters.mEnergy) +
                            " MeV");

    uiManager->ApplyCommand(
            "/run/beamOn " +
            std::to_string(parameters.mBeamParameters.mNumberOfPrimaries));

    userInterface->SessionStart();

    delete userInterface;
    delete visManager;
    delete runManager;

    // I think that Geant4 already deletes those:
    // delete actionInitialisation;
    // delete physicsList;
    // delete detectorConstruction;

    delete rngEngine;
}
