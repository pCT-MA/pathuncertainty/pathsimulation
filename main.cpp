#ifdef G4MULTITHREADED
#include <G4MTRunManager.hh>
#else
#include <G4RunManager.hh>
#endif
#include <G4UImanager.hh>
#include <G4VisExecutive.hh>
#include <QGSP_BIC.hh>

#include "PathActionInitialization.hpp"
#include "PathDetectorConstruction.hpp"
#include "PathPhysicsList.hpp"
#include "PathSimulation.hpp"

int main(int argc, char** argv)
{
    using namespace Path;

    ParameterSet parameters;
    if(argc > 1)
    {
        parameters = XMLParameters(argv[1]);
    }

    // do we really need to set up the RNG?
    auto rngEngine = new CLHEP::RanecuEngine();
    CLHEP::HepRandom::setTheEngine(rngEngine);
    CLHEP::HepRandom::setTheSeed(time(0));

    auto detectorConstruction = new DetectorConstruction(parameters);
    G4VModularPhysicsList* physicsList =
            parameters.mSimulationParameters.mOnlyEMPhysics
                    ? static_cast<G4VModularPhysicsList*>(new PhysicsList())
                    : static_cast<G4VModularPhysicsList*>(new QGSP_BIC(0));
    auto actionInitialisation = new ActionInitialization();

#ifdef G4MULTITHREADED
    G4RunManager* runManager = nullptr;
    if(parameters.mSimulationParameters.mNumberOfThreads > 1)
    {
        G4MTRunManager* multiThreadedRunManager = new G4MTRunManager();
        multiThreadedRunManager->SetNumberOfThreads(
                parameters.mSimulationParameters.mNumberOfThreads);
        runManager = multiThreadedRunManager;
    }

    if(runManager == nullptr)
    {
        runManager = new G4RunManager();
    }
#else
    G4RunManager* runManager = new G4RunManager;
#endif
    auto uiManager = G4UImanager::GetUIpointer();
    auto visManager = new G4VisExecutive("errors");

    runManager->SetVerboseLevel(0);
    runManager->SetUserInitialization(detectorConstruction);
    runManager->SetUserInitialization(physicsList);
    runManager->SetUserInitialization(actionInitialisation);

    visManager->SetVerboseLevel(0);
    visManager->Initialize();
    runManager->Initialize();

    uiManager->ApplyCommand(
            "/analysis/setFileName " +
            parameters.mSimulationParameters.SimulationFileName());

    uiManager->ApplyCommand(
            "/gps/particle " +
            (parameters.mBeamParameters.ParticleIsIon()
                     ? "ion"
                     : parameters.mBeamParameters.mParticleName));
    if(parameters.mBeamParameters.ParticleIsIon())
    {
        uiManager->ApplyCommand(
                "/gps/ion " +
                std::to_string(parameters.mBeamParameters.IonAtomicNumber()) +
                " " +
                std::to_string(parameters.mBeamParameters.IonMassNumber()) +
                " " + std::to_string(parameters.mBeamParameters.IonCharge()));
    }
    if(parameters.mBeamParameters.mFullWidthHalfMaximumInMM > 0)
    {
        uiManager->ApplyCommand("/gps/pos/type Beam");
        uiManager->ApplyCommand("/gps/pos/shape Circle");
        uiManager->ApplyCommand("/gps/pos/radius 0. mm");
        uiManager->ApplyCommand(
                "/gps/pos/sigma_r " +
                std::to_string(
                        parameters.mBeamParameters.mFullWidthHalfMaximumInMM) +
                " mm");
    }
    else
    {
        uiManager->ApplyCommand("/gps/pos/type Point");
    }
    uiManager->ApplyCommand(
            "/gps/pos/centre 0 0 " +
            std::to_string(parameters.mBeamParameters.mSourceZ) + " mm");
    uiManager->ApplyCommand("/gps/ang/type planar");
    uiManager->ApplyCommand("/gps/direction 0 0 1");
    uiManager->ApplyCommand("/gps/ene/mono " +
                            std::to_string(parameters.mBeamParameters.mEnergy) +
                            " MeV");

    uiManager->ApplyCommand(
            "/run/beamOn " +
            std::to_string(parameters.mBeamParameters.mNumberOfPrimaries));

    delete visManager;
    delete runManager;

    // I think that Geant4 already deletes those:
    // delete actionInitialisation;
    // delete physicsList;
    // delete detectorConstruction;

    delete rngEngine;
}
