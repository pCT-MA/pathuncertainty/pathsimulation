#pragma once

#include <G4VSensitiveDetector.hh>

#include "PathHit.hpp"

namespace Path
{
    class SensitiveDetector : public G4VSensitiveDetector
    {
    public:
        SensitiveDetector(std::string const& detectorName);
        virtual ~SensitiveDetector();

        virtual void Initialize(G4HCofThisEvent* HitCollection) override;
        virtual G4bool ProcessHits(G4Step* Step,
                                   G4TouchableHistory* History) override;

    private:
        HitsCollection_t* fHitsCollection;
        G4int fHitCollectionID;
    };
} // namespace Path
