#pragma once

#include <vector>

#include <G4UserRunAction.hh>

namespace Path
{
    class RunAction : public G4UserRunAction
    {
    public:
        RunAction();
        virtual ~RunAction();

        virtual G4Run* GenerateRun() override;

        virtual void BeginOfRunAction(const class G4Run*) override;

        void Fill(int event,
                  double x,
                  double y,
                  double z,
                  double e,
                  bool isPhantom) const;

    private:
        G4int mTracingNTupleId;
        G4int mEventColumnId;
        G4int mXColumnId;
        G4int mYColumnId;
        G4int mZColumnId;
        G4int mEColumnId;
        G4int mIsPhantomColumnId;
    };
} // namespace Path
