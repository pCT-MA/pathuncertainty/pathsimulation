#pragma once

#include <G4Run.hh>

namespace Path
{
    class Run : public G4Run
    {
    public:
        Run();
        virtual ~Run();

        virtual void RecordEvent(const G4Event*) override;

    private:
        /** Stores IDs of sensitive detectors */
        G4int mPhantomHitCollectionId;
        std::vector<G4int> mDetectorHitCollectionIds;
        std::size_t eventNr;
    };
} // namespace Path
