#pragma once

#include <G4VHit.hh>
#include <globals.hh>           // G4int, G4double
#include "G4THitsCollection.hh" // G4HitsCollection
#include "G4ThreeVector.hh"     //  G4ThreeVector

namespace Path
{
    class Hit : public G4VHit
    {
    public:
        Hit();
        Hit(const Hit& CopyHit);
        virtual ~Hit();

        // operators to compare/assign hit objects
        const Hit& operator=(const Hit& CopyHit);
        G4int operator==(const Hit& OtherHit) const;

        inline void* operator new(size_t);
        inline void operator delete(void*);

        /** Causes hits to be displayed as small red circles in visualizer */
        virtual void Draw() override;

        // Set methods to store data
        void SetPosition(G4ThreeVector const& position)
        {
            fPosition = position;
        }
        void SetEnergy(double Energy) { fEnergy = Energy; }
        void SetCopyNr(int copyNr) { fCopyNumber = copyNr; }

        // get methods to retrieve data
        G4ThreeVector const& GetPosition() const { return fPosition; }
        double GetEnergy() const { return fEnergy; }
        int GetCopyNr() const { return fCopyNumber; }

    private:
        G4ThreeVector fPosition;
        double fEnergy;
        G4int fCopyNumber;
    };

    // Over the course of an event, Geant4 can produce multiple hits.
    // These are stored in G4THitsCollection<T> containers.
    // The following are necessary for sensitive detectors to work:

    typedef G4THitsCollection<Hit> HitsCollection_t;
    extern G4ThreadLocal G4Allocator<Hit>* gHitsAllocator;

    inline void* Hit::operator new(size_t)
    {
        if(!gHitsAllocator)
        {
            gHitsAllocator = new G4Allocator<Hit>;
        }
        return (void*)gHitsAllocator->MallocSingle();
    }

    inline void Hit::operator delete(void* hit)
    {
        gHitsAllocator->FreeSingle((Hit*)hit);
    }
} // namespace Path
