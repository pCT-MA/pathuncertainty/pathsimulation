#pragma once

#include <G4VModularPhysicsList.hh>

class G4VPhysicsConstructor;

namespace Path
{
    class PhysicsList : public G4VModularPhysicsList
    {
    public:
        PhysicsList(int verbosity = 0);
        virtual ~PhysicsList();

        virtual void ConstructParticle() override;
        virtual void ConstructProcess() override;

    private:
        std::vector<G4VPhysicsConstructor*> mConstructors;
    };
} // namespace Path
