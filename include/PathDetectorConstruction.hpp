#pragma once

#include "G4VUserDetectorConstruction.hh"
#include "SimulationParameters.hpp"

class G4LogicalVolume;
class G4VPhysicalVolume;

namespace Path
{
    class Detector;
    class SensitiveDetector;

    class World
    {
    public:
        World(WorldParameters const& parameters, float worldLength);

        G4LogicalVolume* GetLogicalVolume();
        G4VPhysicalVolume* GetPhysicalVolume();

    private:
        G4LogicalVolume* mLogicalVolume;
        G4VPhysicalVolume* mPhysicalVolume;
    };

    class Phantom
    {
    public:
        Phantom(G4LogicalVolume* parent,
                PhantomParameters const& parameters,
                float worldWidthHeight);

        std::string GetName() const;

        void SetSensitiveDetector(SensitiveDetector* sensitiveDetector);

    private:
        G4LogicalVolume* mLogicalVolume;
    };

    class Detector
    {
    public:
        Detector(G4LogicalVolume* parent,
                 DetectorParameters const& parameters,
                 float worldSize);
        virtual ~Detector() {}

        std::string const& GetName() const;

        void SetSensitiveDetector(SensitiveDetector* sensitiveDetector);

    private:
        G4LogicalVolume* mLogicalVolume;
    };

    class DetectorConstruction : public G4VUserDetectorConstruction
    {
    public:
        DetectorConstruction(ParameterSet const& parameters);
        virtual ~DetectorConstruction();

        virtual G4VPhysicalVolume* Construct() override;
        virtual void ConstructSDandField() override;

        double GetLayerThickness() const;
        ParameterSet const& GetParameters() const;

    private:
        ParameterSet mParameters;
        World* mWorld;
        Phantom* mPhantom;
        std::vector<Detector*> mDetectors;
    };
} // namespace Path
