#pragma once

#include <G4VUserPrimaryGeneratorAction.hh>

class G4Event;
class G4GeneralParticleSource;

namespace Path
{
    class RunAction;

    class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
    {
    public:
        PrimaryGeneratorAction(RunAction* RunAction);
        virtual ~PrimaryGeneratorAction();

        virtual void GeneratePrimaries(G4Event*) override;

        const G4GeneralParticleSource* GetParticleGun() const
        {
            return fParticleGun;
        }

    private:
        G4GeneralParticleSource* fParticleGun;
    };
} // namespace Path
